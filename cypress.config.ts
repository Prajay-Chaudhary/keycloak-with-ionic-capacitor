import { defineConfig } from 'cypress'

export default defineConfig({
  projectId: 'rqoo5q',
  video: true,
  e2e: {
    'baseUrl': 'http://localhost:8100'
  },
  
  
  component: {
    devServer: {
      framework: 'angular',
      bundler: 'webpack',
    },
    specPattern: '**/*.cy.ts'
  }
  
})