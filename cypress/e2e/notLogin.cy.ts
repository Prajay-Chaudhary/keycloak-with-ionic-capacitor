describe('Test Animal app', () => {
    it('should not login if the form is invalid', () => {
        cy.visit('/');
        cy.contains('Keycloak Example App');
        cy.contains('Login').click();
        cy.get('#username').type('admin');
        cy.contains('Sign In').click();
        cy.url().should('not.include', 'Animal APP');
        cy.wait(3000)
    });
})







