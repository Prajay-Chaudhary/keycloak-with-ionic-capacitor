describe('Test Animal app', () => {

  beforeEach(() => {
    cy.visit('/');
    cy.contains('Keycloak Example App');
    cy.contains('Login').click();
    cy.get('#username').type('admin');
    cy.get('#password').type('admin');
    cy.contains('Sign In').click();
    cy.contains('Get to home page').click();
    cy.url().should('eq', Cypress.config().baseUrl + '/home');
  });



  it('should add an animal', () => {
    // Click the "Add Animal" button
    cy.contains('Add Animal').click();

    // Wait for the modal to appear
    cy.get('ion-toolbar ion-title').should('contain', 'Add Animal'); // Ensure modal title is visible

    // Fill out the form inputs
    cy.get('ion-input[name="name"]').type('Lion');
    cy.get('ion-input[name="type"]').type('Mammal');
    cy.wait(3000)

    // Click the "Add Animal" button within the modal
    cy.get('ion-button[type="submit"]').click();
    cy.wait(3000)

    cy.visit('/home')
    cy.wait(3000)

    // Ensure the modal is not present in the DOM
    cy.get('ion-toolbar ion-title').should('not.contain', 'Add Animal');


    // Click the "Logout" button within the modal
    cy.contains('Logout').click();
    cy.wait(3000)
    cy.url().should('not.include', 'home');

    // Verify that the logout text is not present on the page
    cy.contains('Logout').should('not.exist');



  });
})


