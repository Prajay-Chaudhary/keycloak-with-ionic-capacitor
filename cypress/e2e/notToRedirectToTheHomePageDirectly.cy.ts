describe('Unauthorized access to home page', () => {
    it('should redirect to login page', () => {
        cy.visit('/');
        // Visit the home page directly
        cy.visit('/home');

    });
});
