import { Component } from '@angular/core';
import {NgForm} from '@angular/forms';
import { ModalController } from '@ionic/angular';
import {AnimalService} from 'src/app/services/animal.service';

@Component({
  selector: 'app-create-animal-modal',
  templateUrl: './create-animal-modal.component.html',
  styleUrls: ['./create-animal-modal.component.scss'],
})
export class CreateAnimalModalComponent {

  constructor(private modalController: ModalController, private animalService: AnimalService) { }

  dismiss() {
    this.modalController.dismiss();
  }
  addAnimal(form: NgForm) {
    if (form.valid) {
      const animalData = form.value;
      this.animalService.createAnimal(animalData).subscribe(
        (response: any) => {
          console.log('Animal added successfully:', response);
          this.dismiss();
        },
        (error: any) => {
          console.error('Error adding animal:', error);
          // Optionally, display an error message to the user
        }
      );
    }
  }
}
