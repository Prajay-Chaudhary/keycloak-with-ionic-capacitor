import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Animal } from '../../models/animal.model';
import { AnimalService } from '../../services/animal.service';

@Component({
  selector: 'app-update-animal-modal',
  templateUrl: './update-animal-modal.component.html',
  styleUrls: ['./update-animal-modal.component.scss'],
})
export class UpdateAnimalModalComponent {

  @Input() animal: Animal = { id: 0, name: '', type: '' }; // Initialize with default values

  constructor(
    private modalController: ModalController,
    private animalService: AnimalService
  ) { }

  dismiss() {
    this.modalController.dismiss();
  }

  updateAnimal(form: NgForm) {
    if (form.valid) {
      this.animalService.updateAnimal(this.animal.id, form.value).subscribe(
        (response) => {
          console.log('Animal updated successfully:', response);
          this.dismiss();
        },
        (error) => {
          console.error('Error updating animal:', error);
        }
      );
    }
  }
}
