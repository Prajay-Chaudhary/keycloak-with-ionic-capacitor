import { Component, OnInit } from '@angular/core';
import { AnimalService } from '../../services/animal.service';
import { Animal } from '../../models/animal.model';
import { ModalController } from '@ionic/angular'; 
import { CreateAnimalModalComponent } from 'src/app/modals/create-animal-modal/create-animal-modal.component';
import {UpdateAnimalModalComponent} from 'src/app/modals/update-animal-modal/update-animal-modal.component';

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.scss']
})
export class AnimalListComponent implements OnInit {

  animals: Animal[] = [];

  constructor(
    private animalService: AnimalService,
    private modalController: ModalController 
  ) { }

  ngOnInit(): void {
    this.loadAnimals();
  }

  loadAnimals(): void {
    this.animalService.getAllAnimals().subscribe(animals => {
      console.log('Animals:', animals);
      this.animals = animals;
    });
  }

  updateAnimal(updatedAnimal: Animal) {
    this.animalService.updateAnimal(updatedAnimal.id, updatedAnimal).subscribe(
      (updatedAnimalResponse) => {
        // Find the index of the original animal in the array
        const index = this.animals.findIndex(animal => animal.id === updatedAnimal.id);
        if (index !== -1) {
          // Replace the original animal with the updated animal at the same index
          this.animals[index] = updatedAnimalResponse;
        }
        console.log('Animal updated successfully:', updatedAnimalResponse);
        this.modalController.dismiss(); // Dismiss the modal
      },
      (error) => {
        console.error('Error updating animal:', error);
      }
    );
  }
  
  

  async openCreateAnimalModal() {
    const modal = await this.modalController.create({
      component: CreateAnimalModalComponent
    });
    return await modal.present();
  }

  // Modify animal 
  async openUpdateAnimalModal(animal: Animal) {
    const modal = await this.modalController.create({
      component: UpdateAnimalModalComponent,
      componentProps: {
        animal // Pass the animal object to the modal component
      }
    });
    return await modal.present();
  }

  deleteAnimal(id: number): void {
    this.animalService.deleteAnimal(id).subscribe(() => {
      this.loadAnimals(); // Refresh the list after deletion
    });
  }
}
