import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Animal } from '../models/animal.model'; 

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  private baseUrl = 'http://localhost:9090/animals'; 

  constructor(private http: HttpClient) { }

  getAllAnimals(): Observable<Animal[]> {
    return this.http.get<Animal[]>(this.baseUrl);
  }

  getAnimalById(id: number): Observable<Animal> {
    return this.http.get<Animal>(`${this.baseUrl}/${id}`);
  }

  searchAnimalsByName(name: string): Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.baseUrl}/searchByName?name=${name}`);
  }

  createAnimal(animal: Animal): Observable<any> {
    return this.http.post(this.baseUrl, animal);
  }

  updateAnimal(id: number, updatedAnimal: Animal): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, updatedAnimal);
  }

  deleteAnimal(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}
