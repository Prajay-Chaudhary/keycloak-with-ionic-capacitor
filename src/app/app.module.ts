import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AnimalListComponent} from './components/animal-list/animal-list.component';
import {CreateAnimalModalComponent} from './modals/create-animal-modal/create-animal-modal.component';
import {FormsModule} from '@angular/forms';
import {UpdateAnimalModalComponent} from './modals/update-animal-modal/update-animal-modal.component';
import {AuthConfig, OAuthModule} from 'angular-oauth2-oidc';
import {AuthInterceptor} from './services/auth.interceptor';

@NgModule({
  declarations: [AppComponent, AnimalListComponent, CreateAnimalModalComponent, UpdateAnimalModalComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule, OAuthModule.forRoot()],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }], // Register the interceptor],
  bootstrap: [AppComponent],
})
export class AppModule {}
