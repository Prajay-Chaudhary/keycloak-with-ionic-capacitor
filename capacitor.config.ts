import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'keycloak-with-ionic-capacitor',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
